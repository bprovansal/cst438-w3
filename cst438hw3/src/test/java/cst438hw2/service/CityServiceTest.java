package cst438hw2.service;
 
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.ArgumentMatchers.anyString;

import cst438hw2.domain.*;
 
@SpringBootTest
public class CityServiceTest {

	@MockBean
	private WeatherService weatherService;
	
	@Autowired
	private CityService cityService;
	
	@MockBean
	private CityRepository cityRepository;
	
	@MockBean
	private CountryRepository countryRepository;

	@BeforeEach
    public void setUpEach() {
    	MockitoAnnotations.initMocks( this);
    	cityService = new CityService(cityRepository, countryRepository, weatherService);
    	
    }
	
	@Test
	public void contextLoads() {
	}


	@Test
	public void testCityFound() throws Exception {
		// TODO
		City testCity = new City(1, "CityVille", "Countryvania", "Districtzone", 99991);
		
		given(cityRepository.findById((long)1)).willReturn(testCity);
		given(weatherService.getTempAndTime("CityVille")).willReturn(new TempAndTime(99.99, 0, 909));
		City actualCity = cityService.getCity(1);
	
		assertThat( actualCity ).isEqualTo(testCity);
	}
	
	@Test 
	public void  testCityNotFound() {
		// TODO 
		City expectedCity = new City();
		
		given(cityRepository.findByName("NA").get(0)).willReturn(expectedCity);
		
		City attemptCity = cityRepository.findByName("NA").get(0);
		
		assertThat(attemptCity).isEqualTo(cityRepository.findByName("NA").get(0));
		
	}
	
	@Test 
	public void  testCityMultiple() {
		// TODO 
		
		List<City> expectedList = new ArrayList<City>();
		expectedList.add(new City((long)0, "CityVille", "CC0", "district0", 100));
		expectedList.add(new City((long)1, "CityVille", "CC1", "district1", 101));
		
		given(cityRepository.findByName("CityVille")).willReturn(expectedList);
		
		List<City> attemptList = cityRepository.findByName("CityVille");
		
		assertThat(attemptList).isEqualTo(expectedList);
		
	}

}
