package cst438hw2.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cst438hw2.domain.*;
import jdk.internal.org.jline.utils.Log;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Service
public class CityService {
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private WeatherService weatherService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private FanoutExchange fanout;
	
	public CityService(CityRepository cityRepository, CountryRepository countryRepository, WeatherService weatherService) {
		this.cityRepository = cityRepository;
		this.countryRepository = countryRepository;
		this.weatherService = weatherService;
	}
	
	public CityInfo getCityInfo(String cityName) {
	
		List<City> cityList = cityRepository.findByName(cityName);
		City city = cityList.get(0);
		String cc = city.getCountryCode();
		Country country = new Country(cc, countryRepository.findByCode(cc).getName());
			
		TempAndTime weather = weatherService.getTempAndTime(cityName);
		CityInfo cI = new CityInfo(city, country.getName(), KtoF(weather.temp), MillisToTime(weather.time, weather.timezone));
		return cI;
	}
	
	public City getCity(long id) {
		
		City city = cityRepository.findById(id);
		return city;
	}
	
	public double KtoF(double K) {
		double F = K * (double)(9.0/5.0) - 459.57;
		return Math.floor(F * 100) / 100;
	}
	
	public String MillisToTime(long epoch, int timezone) {
		epoch -= timezone;
		String date = new java.text.SimpleDateFormat("HH:m a").format(new java.util.Date (epoch*1000));
		return date;
	}

	public void requestReservation(String cityName, String level, String email) {
		String msg ="{\"cityName\": \""+ cityName +
				"\" \"level\": \""+level+
				"\" \"email\": \""+email+"\"}" ;
		System.out.println("Sending message:"+msg);
		rabbitTemplate.convertSendAndReceive(fanout.getName(), "", msg);	
	}
	
}
